import { Schema, model } from "mongoose";

const userSchema = new Schema({
  email: { required: true, type: Schema.Types.String, unique: true },
  password: {
    required: true,
    type: Schema.Types.String,
    minlength: 8,
    maxlength: 32,
  },
  files: [{ _id: false, id: Schema.Types.ObjectId, name: Schema.Types.String }],
});

export const User = model("user", userSchema);
