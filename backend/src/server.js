import express from "express";
import bodyParser from "body-parser";
import fileRouter from "./routes/files";
import mongoose from "mongoose";
import userRouter from "./routes/user";
import session from "express-session";

const app = express();
const mongoDbUrl = process.env.MONGO_URL || "mongodb://localhost:27017/test";
const secretKey = process.env.SECRET_KEY || "secret";
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
  session({ secret: secretKey, cookie: { maxAge: 600000 }, resave: false })
);
app.use("/api", fileRouter, userRouter);

mongoose
  .connect(mongoDbUrl)
  .then((res) => {
    console.log("mongodb connected successfully");
    const gfs = new mongoose.mongo.GridFSBucket(res.connection.db);
    app.set("gfs", gfs);
    // const readStream = gfs.openDownloadStream('5f69fa06f4104478d8ae0981')
  })
  .catch((err) => {
    console.error("unable to connect to mongodb", err);
  });

const port = process.env.PORT || 3001;
app.listen(port, () => {
  console.log(`server running at port ${port}`);
});
