import { Router } from "express";
import Mongoose from "mongoose";
import multer from "multer";
import { Readable } from "stream";
import { User } from "../model/user";

const router = Router();
const upload = multer({});

router.get("/file/:fileId", (req, res) => {
  console.log("/GET files");
  const fileId = req.params.fileId;
  const gfs = req.app.get("gfs");

  const downloadStream = gfs.openDownloadStream(
    Mongoose.Types.ObjectId(fileId)
  );
  downloadStream.on("error", (err) => {
    console.log("error downloading file", err);
    res.json({ error: "file download failed" });
  });
  // res.set("Content-Type", "application/pdf");
  res.setHeader("Content-Disposition", `attachment; filename=${fileId}.pdf`);
  downloadStream.pipe(res);
});

router.post("/files", upload.single("file"), (req, res) => {
  console.log("/POST files", req.file);

  if (req.file) {
    const userId = req.body.userid;
    const gfs = req.app.get("gfs");
    const writeStream = gfs.openUploadStream(req.file.originalname);
    const readStream = new Readable();
    readStream.push(req.file.buffer);
    readStream.push(null);
    readStream.pipe(writeStream);
    const file = { id: writeStream.id, name: writeStream.filename };
    writeStream.on("finish", () => {
      User.findByIdAndUpdate(
        { _id: userId },
        {
          $push: { files: [file] },
        },
        { useFindAndModify: false, new: true }
      )
        .then((data) => {
          console.log("data files", data);
          req.session.user = { id: data._id, files: data.files };
          res.json(req.session.user);
        })
        .catch((err) => {
          console.log("error saving file to db: ", err);
          res.json({ error: "error saving file to db" });
        });
    });
    writeStream.on("error", (err) => {
      console.log("error saving file to db: ", err);
      res.json({ error: "error saving file to db" });
    });
  } else res.json({ error: "file not found" });
});

export default router;
