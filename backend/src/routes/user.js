import { Router } from "express";
import { User } from "../model/user";

const router = Router();

router.get("/user", (req, res) => {
  console.log("/GET user");
  res.json(req.session.user);
});
router.post("/signup", (req, res) => {
  console.log("/POST signUp", req.body);
  User.findOne({ email: req.body.email }).then((data) => {
    if (data) res.json({ error: "User already exists" });
    else {
      const newUser = new User({
        email: req.body.email,
        password: req.body.password,
      });
      newUser
        .save()
        .then((data) => {
          res.json({ id: data._id, files: data.files });
        })
        .catch((err) => {
          res.status(500);
          console.log("error occured while signup user", err);
          res.json({ message: "Unable to signup customer" });
        });
    }
  });
});

router.post("/login", (req, res) => {
  console.log("/login", req.body);
  if (!req.session.user) {
    User.findOne({ email: req.body.email }).then((data) => {
      if (!data) {
        console.log("user not found");
        res.json({ error: "Invalid email / password" });
      } else {
        if (data.password !== req.body.password)
          res.json({ error: "Invalid email / password" });
        console.log("user found in db");
        req.session.user = { id: data._id, files: data.files };
        res.json(req.session.user);
      }
    });
  } else {
    res.json(req.session.user);
  }
});

router.get("/logout", (req, res) => {
  if (req.session.user) req.session.user = null;
  res.json(req.session.user);
});

export default router;
